# Inventory Issues

### Problem
<ul>
<li>misreported inventory quantities</li>
<li>unable to fulfill a lot of orders, because stock unavailability</li>
</ul>

#### Problem Analysis
It is cousing by no checkStock handler before payment process,
<br>checkout n items >  (stock items - sold items)
payment process still continue even stock of items is unavailable  


### Solution
create checkStock handler on insert cart and payment process


## STEP BY STEP Deployment
after repository cloned,

```
cd <repo>
```

install composer 

```
composer install
```

setup .env
```
cp .env.example .env (setup with your detail)
```

creat jwt secret
```
php artisan jwt:secret
```

running migration DB and seeder
```
php artisan migrate

php artisan db:seed --class=ProductTableSeeder
```
running Lumen API
```
php -S localhost:8080 -t public
```


open your Postman
let start with user register
methode : POST 
url : 'http://localhost:8080/api/users'
request:
```
{
    "name": "Permana1",
    "email":"permana1@gmail.com",
    "password":"permanapermana",
    "password_confirmation":"permanapermana"
}
```

you will get response:
```
{
    "data": {
        "name": "Permana1",
        "email": "permana1@gmail.com",
        "updated_at": "2021-07-12T07:17:03.000000Z",
        "created_at": "2021-07-12T07:17:03.000000Z",
        "id": 1
    }
}
```


then continue to get Token
Method: POST
url: http://localhost:8080/api/login
request body raw json:
```
{
    "email":"permana1@gmail.com",
    "password":"permanapermana"
}
```
You will get response:
```
{
    "result": true,
    "data": {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA4MFwvYXBpXC9sb2dpbiIsImlhdCI6MTYyNjA3NDQyNywiZXhwIjoxNjI2MDc4MDI3LCJuYmYiOjE2MjYwNzQ0MjcsImp0aSI6IkVpN0dRcVVXRlA2ZmVucEMiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.odgK_MqCHyuIBocGaSHRWqZVZjb9ag1n27pq1BBt6eo",
        "token_type": "bearer",
        "expires_in": ""
    }
}
```

Copy value of token, then continue to list Product<br>
http://localhost:8080/api/product<br>
method GET<br>
Header
Authorization: bearer token

Response
```
{
    "result": true,
    "count": 4,
    "data": [
        {
            "id": 1,
            "product_code": "adi001",
            "product_name": "Sepatu Adidores",
            "product_description": "Sepatu Adidores model terbaru",
            "price": 793263,
            "stock": 14
        },
        {
            "id": 2,
            "product_code": "nk001",
            "product_name": "Sepatu Naiki Ardila",
            "product_description": "Sepatu Naiki Ardila model tercinta",
            "price": 332838,
            "stock": 15
        },
        {
            "id": 3,
            "product_code": "pum001",
            "product_name": "Sepatu Pummo",
            "product_description": "Sepatu Sepatu Pummo model tercinta",
            "price": 683403,
            "stock": 16
        },
        {
            "id": 4,
            "product_code": "kpp001",
            "product_name": "Sepatu Keppo",
            "product_description": "Sepatu Keppo model tercinta",
            "price": 666404,
            "stock": 7
        }
    ]
}
```


Continue to Add Cart<br>
Method "POST" <br>
http://localhost:8080/api/addCart<br>
Header
Authorization: bearer token <br>

request
```
{
    "product_id":1,
    "quantity":1
}
```

Response
```
{
    "result": true,
    "message": "Success Add product to cart",
    "data": [
        {
            "user_id": 1,
            "product_id": 1,
            "product_name": "Sepatu Adidores",
            "quantity": 1,
            "price": 793263,
            "status": "open"
        }
    ]
}
```

Continue to checkout to Process Transaction of your cart<br>
Method POST <br>
http://localhost:8080/api/checkout<br>

Response
```
{
    "result": true,
    "data": {
        "transact_id": 20210712073553001,
        "user_id": 1,
        "status": "process",
        "updated_at": "2021-07-12T07:35:53.000000Z",
        "created_at": "2021-07-12T07:35:53.000000Z",
        "total_price": 793263,
        "vat": 7933
    }
}
```


Continue to Payment to Process Payment Transaction id<br>
Method POST <br>
http://localhost:8080/api/payment<br>
Request
```
{
    "transact_id": 20210712073553001,
    "payment":801196  // total_price + vat
}
```
Response
```
{
    "result": true,
    "data": {
        "status": true,
        "message": "succes payment"
    }
}
```