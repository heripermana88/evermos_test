<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use DB;
use App\Traits\ApiResponser;

class AuthController extends Controller
{
    use ApiResponser;
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return ApiResponser
     */
    public function register(Request $request)
    {
        //validate incoming request 
        $this->validate($request, [
            'email' => 'required|string|email|unique:users',
            'password' => 'required|confirmed',
        ]);

        try 
        {
            $user = new User;
            $user->email= $request->input('email');
            $user->password = app('hash')->make($request->input('password'));
            $user->save();

            return $this->responseData($user);
        } 
        catch (\Exception $e) 
        {
            return response()->json( [
                       'entity' => 'users', 
                       'action' => 'create', 
                       'result' => 'failed'
            ], 409);
        }
    }
	
     /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return ApiResponser
     */	 
    public function login(Request $request)
    {
          //validate incoming request 
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['email', 'password']);

        if (! $token = Auth::attempt($credentials)) {			
            return response()->json(['message' => 'Unauthorized'], 401);
        }
        $type = $request->login_type;
        // return $this->respondWithToken($token);
        return $this->responseData($this->respondWithToken($token)->original);
    }
	
     /**
     * Get user details.
     *
     * @param  Request  $request
     * @return ApiResponser
     */	 	
    public function me(Request $request)
    {
        $user = auth()->user();
        
        return $this->responseData($user);

    }
}