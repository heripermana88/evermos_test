<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Traits\ApiResponser as Response;
use App\Repositories\Cart\CartInterface;

use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    use Response;
    private $cart;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CartInterface $cart)
    {
        $this->cart = $cart;
    }

    public function addCart(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'product_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseValidation($validator->errors());
        }
         
        $cart = $this->cart->addCart($request);

        if($cart){
            if($cart==='out Of Stock')
            return $this->responseOutOfStock($cart);

            return $this->responseData($this->getCart($request), "Success Add product to cart");
        }
        return $this->responseError();
    }

    public function subCart(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'product_id' => 'required',
            'sub_quantity' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseValidation($validator->errors());
        }
         
        $cart = $this->cart->subCart($request);

        if($cart){
            return $this->responseData($cart, "subcart success");
        }
        return $this->responseError();
    }

    public function myCart(Request $request){
        $myCart = $this->getCart($request);
        if (!$myCart->isEmpty()){
            return $this->responseDataCount($myCart);
        }
        return $this->responseDataNotFound('Cart is Empty');
    }

    private function getCart(Request $request){
        return $myCart = $this->cart->myCart($request);
    }
}