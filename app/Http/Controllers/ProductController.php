<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser as Response;
use App\Repositories\Product\ProductInterface;

class ProductController extends Controller
{
    use Response;
    private $product;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ProductInterface $product)
    {
        $this->product = $product;
    }

    public function index(Request $request){
        $product = $this->product->getAll();
        if (!$product->isEmpty()){
            return $this->responseDataCount($product);
        }
        return $this->responseDataNotFound('Data not found');
    
    }

    public function show(Request $request,$product_id){
        $product = $this->product->getDetail($product_id);
        if($product){
            return $this->responseData($product);
        }
        return $this->responseDataNotFound('Data not found');
    
    }
}
