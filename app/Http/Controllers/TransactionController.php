<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser as Response;
use App\Repositories\Transact\TransactInterface;
use App\Repositories\Cart\CartInterface;
use App\Repositories\Payment\PaymentInterface;

class TransactionController extends Controller
{
    use Response;
    private $transact;
    private $cart;
    private $payment;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TransactInterface $transact, CartInterface $cart,PaymentInterface $payment)
    {
        $this->transact = $transact;
        $this->cart = $cart;
        $this->payment = $payment;
    }

    public function checkout(Request $request){
        $cart = $this->cart->myCart($request);
        if(count($cart)<1)
        return $this->responseError('no Cart Items');

        $checkout = $this->transact->checkout($request,$cart);
        
        if(!$checkout)
        return $this->responseError('checkout Failed');

        return $this->responseData($checkout);
    }

    public function payment(Request $request){
        $transaction = $this->transact->detail($request);

        if(!$transaction)
        return $this->responseDataNotFound('Transaction ID not found');

        if($request->payment !== ($transaction->total_price + $transaction->vat))
        return $this->responseError('payment value not valid');
        
        $paymentProcess = $this->payment->paymentProcess($request);
        
        if(!$paymentProcess)
        return $this->responseError('payment failed');

        return $this->responseData($paymentProcess);

    }
}