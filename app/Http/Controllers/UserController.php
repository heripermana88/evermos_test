<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Return list of Users
     * @return Iluminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        
        return $this->responseData($users);
    }
    
    /**
     * Create User
     * @return Iluminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:100',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|confirmed',
        ];

        $this->validate($request,$rules);

        $fields = $request->all();
        $fields['password'] = Hash::make($request->password);

        $user = User::create($fields);

        return $this->successResponse($user, Response::HTTP_CREATED);
    }
    
    /**
     * Obtain and show one User
     * @return Iluminate\Http\Response
     */
    public function show($user)
    {
        $user = User::findOrFail($user);

        return $this->successResponse($user);
    }

    /**
     * Update an existing User
     * @return Iluminate\Http\Response
     */
    public function update(Request $request, $user)
    {
        $rules = [
            'name' => 'required|max:100',
            'email' => 'required|email|unique:users,email,'.$user,
            'password' => 'required|min:8|confirmed',
        ];

        $this->validate($request,$rules);

        $user = User::findOrFail($user);
        $user->fill($request->all());

        if($request->has('password')){
            $user->password = Hash::make($request->password);
        }
        
        if($user->isClean()) {
            return $this->errorResponse('At lieast one value must changes', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        
        $user->save();
        return $this->successResponse($user);
    }
    
    /**
     * Delete existing of Users
     * @return Iluminate\Http\Response
     */
    public function destroy($user)
    {
        $user = User::findOrFail($user);

        $user->delete();

        return $this->successResponse($user);
    }
}
