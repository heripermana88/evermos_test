<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_name', 'product_description', 'price'
    ];

    protected $hidden = [
      'created_at','updated_at'
    ];

    public function scopecheckStock($query,$product_id,$add=1,$cart_q=0){
      $stock = $query->select('stock')->where('id',$product_id)->first();

      if($stock && $stock->stock >= ($add + $cart_q)){
        return true;
      }

      return false;
    }
}
