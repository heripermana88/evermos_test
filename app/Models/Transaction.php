<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Transaction extends Model
{
    protected $primaryKey ='transact_id';
    public $incrementing = false;
    protected $casts = [
        'transact_id' => 'integer', 
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transact_id', 'user_id', 'total_price','vat'
    ];

    public function scopesuccesPaid($query, $transact_id){
        DB::beginTransaction();
        $transact_detail = transactionDetail::select('transaction_details.*', 'p.stock')->leftJoin('products as p','p.id','=','product_id')->where('transact_id',$transact_id)->where('status','process')->get();
        
        $update_td = true;
        $message='succes payment';
        
        foreach ($transact_detail as $key => $td) {
            if($td['quantity'] <= $td['stock']){
                transactionDetail::where('product_id',$td['product_id'])->where('transact_id',$transact_id)->where('status','process')->update(['status'=>'paid']);
            }else{
                $update_td=false;
                $message='Some Items out Of Stock';
                break;
            }
        }

        $update_transaction = Transaction::where('transact_id',$transact_id)->update(['status'=>'paid','paid_date' => date('Y-m-d H:i:s')]);

        if($update_td && $update_transaction){
            DB::commit();
            return [
                'status' => true,
                'message' => $message
            ];
        }else{
            DB::rollback();
            return [
                'status' => false,
                'message' => $message
            ];
        }
    }
}