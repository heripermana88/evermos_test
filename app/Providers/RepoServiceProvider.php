<?php

namespace App\Providers;

use App\Repositories\Product\EloquentProductRepository;
use App\Repositories\Product\ProductInterface;
use App\Repositories\Cart\EloquentCartRepository;
use App\Repositories\Cart\CartInterface;
use App\Repositories\Transact\EloquentTransactRepository;
use App\Repositories\Transact\TransactInterface;
use App\Repositories\Payment\EloquentPaymentRepository;
use App\Repositories\Payment\PaymentInterface;


use Illuminate\Support\ServiceProvider;

class RepoServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(ProductInterface::class, EloquentProductRepository::class);
        $this->app->bind(CartInterface::class, EloquentCartRepository::class);
        $this->app->bind(TransactInterface::class, EloquentTransactRepository::class);
        $this->app->bind(PaymentInterface::class, EloquentPaymentRepository::class);
    }
}
