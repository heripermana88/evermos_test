<?php

namespace App\Repositories\Cart;

use Illuminate\Http\Request;

interface CartInterface
{
    public function addCart(Request $request);
    public function subCart(Request $request);
    public function myCart(Request $request);
}