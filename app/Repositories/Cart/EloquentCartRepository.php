<?php
namespace App\Repositories\Cart;

use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Product;

class EloquentCartRepository implements CartInterface
{
    public function addCart(Request $request){
        $cart = Cart::where('product_id',$request->product_id)->where('user_id',$request->user_id)->where('status','open')->first();
        
        if($cart){
            if(!Product::checkStock($request->product_id,$request->quantity,$cart->quantity))
            return 'out Of Stock';

            return $cart->update(['quantity' => $cart->quantity + $request->quantity]);
        }else{
            if(!Product::checkStock($request->product_id,$request->quantity))
            return 'out Of Stock';

            $addCart = new Cart();
            $addCart->user_id = $request->user_id;
            $addCart->product_id = $request->product_id;
            $addCart->quantity = $request->quantity;
            
            return $addCart->save();
        }
    }

    public function subCart(Request $request){
        $cart = Cart::where('product_id',$request->product_id)->where('user_id',$request->user_id)->where('status','open')->first();
        
        if(!$cart)
        return false;
        
        if($cart->quantity > $request->sub_quantity){
            $cart->update(['quantity' => $cart->quantity - $request->sub_quantity]);
        }elseif($cart->quantity == $request->sub_quantity){
            $cart->delete();
        }

        $myCart = Cart::where('user_id',$request->user_id)->where('status','open')->get();
        return $myCart;

    }

    public function myCart(Request $request){
        $myCart = Cart::select('cart.user_id','cart.product_id','product_name','quantity','price','status')->leftJoin('products','products.id','=','cart.product_id')->where('user_id',$request->user_id)->where('status','open')->get();
        return $myCart;
    }
}