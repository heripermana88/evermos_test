<?php
namespace App\Repositories\Payment;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\Transaction;

class EloquentPaymentRepository implements PaymentInterface
{
    private $model;

    function __construct(Payment $model){
        $this->model = $model;
    }

    public function paymentProcess(Request $request){
        $payment = new Payment();
        $payment->transact_id = $request->transact_id;
        $payment->total = $request->payment;

        if($this->payment($request->all())){
            $payment->status = 'paid';
        }else{
            $payment->status = 'failed';
        }
        $payment->save();

        if($payment->status === 'paid'){
            $paid_status = Transaction::succesPaid($request->transact_id);
            return $paid_status;
        }

        return false;
    }

    public function paymentRefund(Request $request){

    }

    private function payment($data){
        
        return true;
    }
}