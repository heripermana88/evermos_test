<?php

namespace App\Repositories\Payment;

use Illuminate\Http\Request;

interface PaymentInterface
{
    public function paymentProcess(Request $request);
    public function paymentRefund(Request $request);
}