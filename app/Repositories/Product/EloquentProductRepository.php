<?php
namespace App\Repositories\Product;

use Illuminate\Http\Request;
use App\Models\Product;

class EloquentProductRepository implements ProductInterface
{
    private $model;

    function __construct(Product $model){
        $this->model = $model;
    }

    public function getAll(){
        return $this->model::all();
    }

    public function getDetail($product_id){
        return $this->model::where('id',$product_id)->first();
    }
}