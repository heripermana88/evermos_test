<?php

namespace App\Repositories\Product;

use Illuminate\Http\Request;

interface ProductInterface
{
    public function getAll();
    public function getDetail($product_id);
}