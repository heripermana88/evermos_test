<?php
namespace App\Repositories\Transact;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\transactionDetail;
use App\Models\Product;
use App\Models\Cart;
use DB;

class EloquentTransactRepository implements TransactInterface
{
    public function checkout(Request $request, $cart){
        if(count($cart)==0)
        return false;

        $td=true;

        DB::beginTransaction();
        $transact_id = Date('YmdHis').$this->serialGen($request->user_id,3);
        
        $transaction = new Transaction();
        $transaction->transact_id = $transact_id;
        $transaction->user_id = $request->user_id;
        $transaction->status = 'process';
        $transaction->save();

        $total_price =0;
        foreach ($cart as $kc => $c) {
            $transact_detail = new transactionDetail();
            $transact_detail->transact_id = $transact_id;
            $transact_detail->product_id = $c['product_id'];
            $transact_detail->quantity = $c['quantity'];
            $transact_detail->price = $c['price'];
            $transact_detail->status = 'process';
            $transact_detail->save();

            if(!$transact_detail){
                $td=false;
                break;
            }else{
                
            }

            $total_price += $c['quantity'] * $c['price'];
        }

        $transaction->total_price = $total_price;
        $transaction->vat = round($total_price/100);

        Cart::where('status','open')->where('user_id',$request->user_id)->update(['status'=>'process']);
        
        if($td && $transaction->update()){
            DB::commit();
            return $transaction;
        }else{
            DB::rollback();
            return false;
        }
    }

    public function detail(Request $request){
        $transaction = Transaction::where('user_id',$request->user_id)->where('transact_id',$request->transact_id)->where('status','process')->first();

        return $transaction;
    }

    private function serialGen($int,$len){
        $con='';
        for ($i=0; $i < $len; $i++) { 
            $con.='0';
        }
        return substr($con.$int,-$len);
    }
}