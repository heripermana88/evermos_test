<?php

namespace App\Repositories\Transact;

use Illuminate\Http\Request;

interface TransactInterface
{
    public function checkout(Request $request, $cart);
    public function detail(Request $request);
}