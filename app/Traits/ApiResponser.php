<?php
namespace App\Traits;

use Illuminate\Http\JsonResponse;

trait ApiResponser
{
    public function responseData ($data, $message = null){
        if ($message != null){
            return new JsonResponse([
                'result' => true,
                'message' => $message,
                'data'   => $data,
            ],200);
        }
        return new JsonResponse([
            'result' => true,
            'data'      => $data,
        ],200);
    }

    public function responseError($message = null, $status = 500){
        if ($message != null){
            return new JsonResponse([
                'result' => false,
                'message' => $message,
            ],$status);
        }
        return new JsonResponse([
            'result'    => false,
            'mesage'    => $message,
        ],$status);
    }


    public function responseDataCount($data, $count = null){
        if($count == null){
            return new JsonResponse([
                'result'    => true,
                'count'      => count($data),
                'data'      => $data
            ],200);
        }else{
            return new JsonResponse([
                'result' => true,
                'count' => $count-1,
                'data' => $data
            ], 200);
        }
    }

    public function responseValidation($validation, $data = null){
        return new JsonResponse([
            'result' => false,
            'data' => $data,
            'message' => $validation,
        ], 422);
    }

    public function responseOutOfStock($data = null){
        return new JsonResponse([
            'result' => false,
            'data' => $data,
            'message' => 'uot Of Stock',
        ], 422);
    }

    public function responseDataNotFound($customMessage = "",$detail = "",$lang =""){
        $statusCode = 400;
        if ($customMessage == "") {
            switch ($lang) {
                case "en" :
                    $info = "Data not found";
                    break;
                default :
                    $info = "Data tidak ditemukan";
            }
        }else{
            $info = $customMessage;
        }
        if ($detail == "") {
            return new JsonResponse([
                'info' => $info,
            ], $statusCode);
        } else {
            return new JsonResponse([
                'info' => $info,
                'detail' => $detail,
            ], $statusCode);
        }
    }

    /**
     * Build success Response
     * @param string|array $data
     * @param int $code
     * @return Illuminate\Http\Response
     */
    public function successResponse($data, $code = JsonResponse::HTTP_OK)
    {
        return response()->json(['data' => $data],$code);
    }

        /**
     * Build error Response
     * @param string|array $message
     * @param int $code
     * @return Illuminate\Http\Response
     */
    public function errorResponse($message, $code)
    {
        return response()->json(['error' => $message, 'code' => $code],$code);
    }

    public function respondWithToken($token,$code = JsonResponse::HTTP_OK)
    {
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => ''
        ],$code);
    }
}