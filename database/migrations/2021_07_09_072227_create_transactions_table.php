<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigInteger('transact_id')->unsigned();
            $table->integer('user_id')->index();
            $table->mediumInteger('total_price')->unsigned()->nullable();
            $table->mediumInteger('vat')->unsigned()->nullable();
            $table->enum('status',['process','paid','expired'])->default('process')->index();
            $table->dateTime('paid_date')->nullable();
            $table->timestamps();

            $table->primary(array('transact_id', 'user_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
