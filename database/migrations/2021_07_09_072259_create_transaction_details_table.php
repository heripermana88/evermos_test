<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->bigInteger('transact_id')->unsigned();
            $table->bigInteger('product_id')->index();
            $table->tinyInteger('quantity')->unsigned();
            $table->mediumInteger('price')->unsigned();
            $table->enum('status',['process','paid','expired'])->default('process')->index();
            $table->timestamps();

            $table->primary(array('transact_id', 'product_id'));
            $table->foreign('transact_id')->references('transact_id')->on('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
