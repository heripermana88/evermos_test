<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = [
            [
               'product_name' => 'Sepatu Adidores', 
               'product_code' => 'adi001', 
               'product_description' => 'Sepatu Adidores model terbaru',
               'price' => rand(100000,999999), 
               'stock' => rand(1,20) 
            ],
            [
               'product_name' => 'Sepatu Naiki Ardila', 
               'product_code' => 'nk001', 
               'product_description' => 'Sepatu Naiki Ardila model tercinta',
               'price' => rand(100000,999999), 
               'stock' => rand(1,20) 
            ],
            [
               'product_name' => 'Sepatu Pummo', 
               'product_code' => 'pum001', 
               'product_description' => 'Sepatu Sepatu Pummo model tercinta',
               'price' => rand(100000,999999), 
               'stock' => rand(1,20) 
            ],
            [
               'product_name' => 'Sepatu Keppo', 
               'product_code' => 'kpp001', 
               'product_description' => 'Sepatu Keppo model tercinta',
               'price' => rand(100000,999999), 
               'stock' => rand(1,20) 
            ]
        ];

        foreach ($product as $k => $p) {
            Product::create($p);
        }
    }
}
