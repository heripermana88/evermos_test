<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['middleware' => 'auth','prefix' => 'api'], function ($router) 
{
    $router->get('userDetail', 'AuthController@me');
});

$router->group(['prefix' => 'api'], function () use ($router) 
{
    $router->group(['middleware' => 'auth'], function ($router) 
    {
        $router->post('addCart', 'CartController@addCart');
        $router->get('myCart', 'CartController@myCart');
        $router->get('subCart', 'CartController@subCart');

        $router->post('checkout', 'TransactionController@checkout');
        $router->post('payment', 'TransactionController@payment');
    });

    $router->post('register', 'AuthController@register');
    $router->post('login', 'AuthController@login');

    $router->get('product', 'ProductController@index');
    $router->get('product/{product_id}', 'ProductController@show');

    /**
    * User Route
    */
    $router->get('/users', 'UserController@index');
    $router->post('/users', 'UserController@store');
    $router->get('/users/{user}', 'UserController@show');
    $router->put('/users/{user}', 'UserController@update');
    $router->patch('/users/{user}', 'UserController@update');
    $router->delete('/users/{user}', 'UserController@destroy');
});